<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="site-content" role="main">
			<main>

				<?php
					$feature_image = get_field('feature_image');
				?>
				<div id="feature-image" style="position: relative;">
					<img class="img-full" src="<?php echo $feature_image['url']; ?>" alt="<?php echo $feature_image['alt']; ?>">
					<div class="overlay d-none d-md-flex">
						<h1 class="text-white">Experience. Leadership. Proven Results.</h1>
						<div class="buttons mt-4">
							<a id="involved-botton" class="btn-white mr-0 mr-md-2" href="javascript:void(0);">GET INVOLVED</a>
							<a href="#about" class="btn-white">LEARN MORE</a>
						</div>
					</div>
				</div>

				<?php get_template_part( '/components/acf-flexible-layout/layouts' ); ?>

			</main>
		</div>
	</div>

	<div class="modal-wrapper">
		<div>
			<div class="modal-content">
				<?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true tabindex=49]
	'); ?>
				<div class="close">
					<i class="fa fa-times" aria-hidden="true"></i>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>