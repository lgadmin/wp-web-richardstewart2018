// Windows Ready Handler

//=require ../components/nav.js 

repositionStickyHeader();
RepositionFeatureItem([],'#feature-image', null, 1600);

$('a').on('click', function(e){
	
	if($(this).attr('href').startsWith('#')){
		e.preventDefault();

		var scrollTo = $(this).attr('href');
		var settings = {
	        duration: 2 * 1000,
	        offset: ($('.site-header').outerHeight()) * -1
	    };

	    headerScrollTo(scrollTo, settings);
	}
});

$('#involved-botton').on('click', function(){
	$('.modal-wrapper').fadeIn();
	$('body').addClass('open');
});

$('.modal-wrapper .close').on('click', function(){
	$(this).closest('.modal-wrapper').fadeOut();
	$('body').removeClass('open');
});