$('.mobile-toggle').on('click', function(){
	$('.navbar-collapse').toggleClass('collapse');
});

if($('.mobile-toggle').is(':visible')){
	$('.navbar-collapse a').on('click', function(){
		$(this).closest('.navbar-collapse').toggleClass('collapse');
	});
}

/*-----------------------------------*/

$('#site-navigation ul > li > a').on('click', function(e){
	e.preventDefault();

	var toElement = $(this).attr('href');
	var settings = {
        duration: 2 * 1000,
        offset: ($('.site-header').outerHeight()) * -1
    };

    headerScrollTo(toElement, settings);

});