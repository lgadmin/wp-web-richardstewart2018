(function($) {

        function RepositionFeatureItem(fixItemSelector, targetItemSelector, callback, tartgetItemMinWidth) {
        var windowWidth = window.innerWidth;
        var windowHeight = window.innerHeight;
        var targetElements = document.querySelectorAll(targetItemSelector);

        var mobileBreakPoint = 480;
        var tabletBreakPoint = 768;
        var desktopBreakPoint = 1280;

        if(fixItemSelector.length > 0){
            var fixItemHeightArray = fixItemSelector.map(function(querySelector){
                var items = document.querySelectorAll(querySelector);

                if(items.length > 0){
                    var itemsHeightArray = Array.prototype.map.call(items,function(item){
                        return item.offsetHeight;
                    });
                    var itemsHeight = Array.prototype.reduce.call(itemsHeightArray, function(total, single){
                        return total + single;
                    });
                    return itemsHeight;
                }else{
                    return 0;
                }
            });

            var fixItemHeight = fixItemHeightArray.reduce(function(total, single){
                return total + single;
            });
        }else{
            var fixItemHeight = 0;
        }

        // Append Parent to target element
        Array.prototype.every.call(targetElements, function(item){
            var parent = item.parentNode;
            var wrapper;

            if(parent.className == 'k-feature-item'){
                wrapper = parent;
            }else{
                wrapper = document.createElement('div');

                wrapper.setAttribute('class', 'k-feature-item');
                // set the wrapper as child (instead of the element)
                parent.replaceChild(wrapper, item);
                // set element as child of wrapper
                wrapper.appendChild(item);
            }
    
            var marginTop = (windowHeight - item.offsetHeight) / 2;
            if (marginTop > 0) {
                marginTop = 0;
            }

            if(item.offsetWidth < tartgetItemMinWidth){
                item.style.width = tartgetItemMinWidth;
            }

            wrapper.style.overflowX = 'hidden';
            wrapper.style.overflowY = 'hidden';

            if (item.offsetHeight + fixItemHeight < windowHeight) {
                wrapper.style.height = 'auto';
                item.style.marginTop = '0px';
            } else if (windowWidth > tabletBreakPoint) {
                wrapper.style.height = (windowHeight - fixItemHeight) + 'px';
                item.style.marginTop = marginTop + 'px';
            }
        });

        if(callback != null){
            callback();
        }
    }

    //=require components/helper.js 
    $(document).ready(function(){

        //=require window-event/ready.js 
        
    });

    $(window).on('load', function(){

        //=require window-event/load.js 

    });

    $(window).on('scroll', function(){

        //=require window-event/scroll.js 

    })

    $(window).on('resize', function(){

        //=require window-event/resize.js 

    })

}(jQuery));