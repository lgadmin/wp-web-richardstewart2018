<?php

	global $lg_tinymce_text_color;
	global $lg_tinymce_button;

	$colors = array('white', 'dark', 'red');
	$color_array = array();
	$button_array = array();


	if($colors && is_array($colors)){
		foreach ($colors as $key => $value) {
			array_push($color_array, array(
				'title' => ucwords($value),
	            'inline' => 'span',
	            'classes' => 'text-'.$value
			));
		}
	}

	if($colors && is_array($colors)){
		foreach ($colors as $key => $value) {
			array_push($button_array, array(
				'title' => ucwords($value),
	            'selector' => 'a',
	            'classes' => 'btn btn-'.$value
			));
		}
	}

	$lg_tinymce_text_color = array(
        'title' => 'Colors',
        'items' =>  $color_array
    );
    $lg_tinymce_button = array(
        'title' => 'Buttons',
        'items' =>  $button_array
    );

?>