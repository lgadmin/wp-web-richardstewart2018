<?php

	global $lg_tinymce_font_size;

	$title_size = array('h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'small');
	$title_size_array = array();

	if($title_size && is_array($title_size)){
		foreach ($title_size as $key => $value) {
			array_push($title_size_array, array(
				'title' => ucwords($value),
	            'selector' => 'span,div,h1,h2,h3,h4,h5,h6,p,a',
	            'classes' => $value
			));
		}
	}
    
    $lg_tinymce_font_size = array(
        'title' => 'Font Size',
        'items' =>  $title_size_array
    );

?>