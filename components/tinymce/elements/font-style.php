<?php

	global $lg_tinymce_font_style;

	$title_style = array('text-left', 'text-center', 'text-right', 'text-lowercase', 'text-uppercase', 'text-capitalize', 'font-weight-light', 'font-weight-normal', 'font-weight-bold');
	$title_style_array = array();

	if($title_style && is_array($title_style)){
		foreach ($title_style as $key => $value) {
			array_push($title_style_array, array(
				'title' => ucwords($value),
	            'selector' => 'span,div,h1,h2,h3,h4,h5,h6,p,a',
	            'classes' => $value
			));
		}
	}
    
    $lg_tinymce_font_style = array(
        'title' => 'Font style',
        'items' =>  $title_style_array
    );

?>