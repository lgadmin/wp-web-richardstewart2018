<?php

	/* Include theme functions */
	$function_path = get_stylesheet_directory() . '/functions';

	$file_path = array(
		'/theme-supports.php',
		'/enqueue_styles_scripts.php',
		'/helper.php',
		'/custom-post-types.php',
		'/custom-taxonomy.php',
		'/custom-menus.php',
		'/nav-walker.php',
		'/include/template-override.php',
		'/include/image-optimization.php'
	);

	foreach ($file_path as $key => $value) {
		if (file_exists($function_path . $value)) {
		    require_once($function_path . $value);
		}
	}
	/* end */


	require_once get_stylesheet_directory() . '/components/main.php';
?>