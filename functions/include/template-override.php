<?php

function archive_template( $pathArray ){

    $templateRoot = get_stylesheet_directory();

    for ($x = 0; $x < sizeof($pathArray); $x++) {
        $file = $templateRoot . $pathArray[$x];
        if(file_exists($file))
            return $file;
    } 

    return false;

}

function lg_single_template( $single_template ) {

    global $post;

    $paths = array(
        '/templates/single/'.$post->post_type.'-'.$post->post_name.'.php',
        '/templates/single/'.$post->post_type.'.php',
        '/single.php',
        '/singlular.php',
        '/index.php'
    );
    
    $template = archive_template($paths);

    if($template)
        return $template;
    else
        return $single_template;

}

function lg_page_template( $page_template ) {

    global $post;

    $page_template = get_post_meta($post->ID,'_wp_page_template',true);

    if($page_template == '' || $page_template == 'default'){
        $paths = array(
            '/templates/page/'.$post->post_name.'.php',
            '/templates/page/'.$post->ID.'.php',
            '/page.php',
            '/singular.php',
            '/index.php'
        );
        
        $template = archive_template($paths);

        if($template)
            return $template;
        else
            return $page_template;
    }

    return get_stylesheet_directory() . '/' . $page_template;

}

function lg_taxonomy_template( $taxonomy_template ){
    $tax = get_queried_object();

    $paths = array(
        '/templates/taxonomy/'.$tax->taxonomy.'-'.$tax->slug.'.php',
        '/templates/taxonomy/'.$tax->taxonomy.'.php',
        '/taxonomy.php',
        '/archive.php',
        '/index.php'
    );
    
    $template = archive_template($paths);

    if($template)
        return $template;
    else
        return $taxonomy_template;
}

function lg_archive_template( $archive_template ) {
    $archive = get_queried_object();

    $paths = array(
        '/templates/archive/'.$archive->name.'.php',
        '/archive.php',
        'index.php'
    );
    
    $template = archive_template($paths);

    if($template)
        return $template;
    else
        return $archive_template;
}

add_filter( 'single_template', 'lg_single_template' ) ;
add_filter( "page_template", "lg_page_template" ); 
add_filter( "taxonomy_template", "lg_taxonomy_template" );
add_filter( 'archive_template', 'lg_archive_template' ) ;

?>